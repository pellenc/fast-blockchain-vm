#pragma once

#include <stddef.h>
#include <stdint.h>

struct config {
	size_t num_accounts; // Number of accounts in the memory
	size_t cache_size; // Recent addresses we keep track of to add contention
	uint32_t initial_balance; // Initial balance for every account
	unsigned int seed; // Seed for random number generation
	float payment_tx_prob; // For now unused, in the future: prob for every kind of transaction
	float recent_account_prob; // Probability we pick a recent address, to add contention
};

extern struct config config;

// Later could fetch config from file
void get_config();
