#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "generator.h"
#include "config.h"

struct state {
	size_t recent_addr_idx;
	uint64_t* recent_addrs;
};

struct state state;

uint64_t uniform(uint64_t start, uint64_t end) {
	// Note: rand() only generates 32 random bits, see if it is problematic
	return (rand() % (end - start)) + start;
}

bool random_bool(float probability) {
	return rand() < probability * ((float) RAND_MAX);
}

uint64_t next_addr() {
	uint64_t res;
	if (random_bool(config.recent_account_prob)) {
		res = state.recent_addrs[rand() % config.cache_size];
	} else {
		res = rand() % config.num_accounts;
	}

	state.recent_addrs[state.recent_addr_idx] = res;
	state.recent_addr_idx = (state.recent_addr_idx + 1) % config.cache_size;
	return res;
}

tx_t next_transaction() {
	tx_t res;
	res.type = PAYMENT;
	
	struct tx_payment payment;
	payment.amount = uniform(0, config.initial_balance / 4);
	payment.from = next_addr();
	payment.to = next_addr();
	if (payment.from == payment.to) {
		payment.to++;
	}
	
	res.transaction.payment = payment;
	return res;
}

void init_generator() {
	state.recent_addr_idx = 0;
	state.recent_addrs = calloc(config.cache_size, sizeof(uint64_t));
	if (state.recent_addrs == NULL) {
		printf("Failed to allocate memory");
		exit(1);
	}

	for (size_t i = 0; i < config.cache_size; i++) {
		state.recent_addrs[i] = i;
	}
}

void clean_generator() {
	free(state.recent_addrs);
	state.recent_addrs = NULL;
}
