#pragma once

#include <stdint.h>

enum tx_enum { PAYMENT, AUCTION };

struct tx_payment {
     uint64_t  from;
     uint64_t  to;
     uint32_t  amount;
};

struct tx_auction {
     uint64_t  from;
     uint64_t  target;
     uint32_t  amount;
};

typedef struct {
	enum tx_enum type;
	union {
		struct tx_payment payment;
		struct tx_auction auction;
	} transaction;
} tx_t;
