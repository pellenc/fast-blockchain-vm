#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "transactions.h"
#include "generator.h"
#include "config.h"


// Measurement of rate of generating new transactions
void time_tx_gen() {
	printf("Starting measurement of transaction generation\n");
	
	time_t start = time(NULL);
	double elapsed = 0;
	size_t n[] = {0, 0}; // Count each transaction type generated
	const size_t bytes[] = {20, 20}; // Number of bytes for each transaction type
	do {
		for (int i = 0; i < 10000; i++) {
			tx_t tx = next_transaction();
			n[tx.type]++;
		}
		elapsed = difftime(time(NULL), start);
	} while (elapsed < 5.);

	size_t total_bytes = n[0] * bytes[0] + n[1] * bytes[1];
	double mbps = ((double) total_bytes) / (elapsed * 1048576);
	printf("Generated: \n"
		   "\t * %lu payments,\n"
		   "\t * %lu auction bids\n"
		   "for a total of %lu bytes in %.2f seconds, or %.f Mbps.\n",
		   n[0], n[1], total_bytes, elapsed, mbps);
}

// Check random generation by printing a few transactions
void print_some_tx() {
	for (int i = 0; i < 20; i++) {
		tx_t tx = next_transaction();
	
		switch (tx.type) {
		case PAYMENT:
			{
				struct tx_payment payment = tx.transaction.payment;
				printf("Payment from %lu to %lu with amount: %u\n", payment.from, payment.to, payment.amount);
				break;
			}
		case AUCTION:
		default:
			{
				printf("Transaction not handled\n");
			}
		}
	}
}

int main(void) {
	get_config();
	init_generator();
	
	if (config.seed == 0) {
		srand(time(NULL)); // no seed
	} else {
		srand(config.seed);
	}

	print_some_tx();
	time_tx_gen();
	clean_generator();

	return 0;
}
