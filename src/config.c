#include "config.h"

struct config config;

void get_config() {
	config.num_accounts = 2147483647;
	config.cache_size = 200;
	config.initial_balance = 10000;
	config.seed = 400;
	config.payment_tx_prob = 0.95f;
	config.recent_account_prob = 0.5f;
}
